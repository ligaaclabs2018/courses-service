package org.waterford.ligaaclabs.courses.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/*
Will mark this class as a configuration one. Configurations can be made using annotation, implementing configuration
interfaces or by defining bean providing methods ( See @Bean )
*/
@Configuration
@ComponentScan("org.waterford.ligaaclabs.courses.service.*")
public class ServiceConfiguration {
}
