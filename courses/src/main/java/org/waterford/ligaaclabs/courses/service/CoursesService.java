package org.waterford.ligaaclabs.courses.service;

import org.waterford.ligaaclabs.courses.entities.model.Course;

import java.util.List;
import java.util.Optional;

public interface CoursesService {
    List<Course> getAll();

    Optional<Course> getById(int id);

    List<Course> getBySubjectId(int subjectId);

    List<Course> getByTeacher(String teacher);

    Course saveCourse(Course course);

    void deleteById(int id);
}
