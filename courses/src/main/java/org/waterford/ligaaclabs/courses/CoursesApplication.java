package org.waterford.ligaaclabs.courses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.waterford.ligaaclabs.courses.entities.config.EntitiesConfiguration;
import org.waterford.ligaaclabs.courses.service.config.ServiceConfiguration;
import org.waterford.ligaaclabs.courses.webapp.config.WebappConfiguration;

@SpringBootApplication
@Configuration
@Import({EntitiesConfiguration.class, ServiceConfiguration.class, WebappConfiguration.class})
public class CoursesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoursesApplication.class, args);
	}
}
