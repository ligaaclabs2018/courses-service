package org.waterford.ligaaclabs.courses.webapp.controllers;

import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import org.waterford.ligaaclabs.courses.entities.model.Course;
import org.waterford.ligaaclabs.courses.service.CoursesService;
import org.waterford.ligaaclabs.courses.webapp.exceptions.CourseNotFoundException;
import org.waterford.ligaaclabs.courses.webapp.transport.CourseTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequestMapping("/courses")
@RestController
public class CoursesController {
    @Inject
    private CoursesService coursesService;
    @Inject
    private ConversionService conversionService;


    @GetMapping
    public List<CourseTO> getAll() {
        return coursesService.getAll().stream().map(course -> conversionService.convert(course, CourseTO.class)).collect(Collectors.toList());
    }

    @GetMapping(path = "/{id}")
    public CourseTO getById(@PathVariable("id") int id) {
        Optional<Course> course = coursesService.getById(id);
        if (!course.isPresent()) {
            throw new CourseNotFoundException();
        }
        return conversionService.convert(course.get(), CourseTO.class);
    }

    @PutMapping
    public CourseTO saveCourse(@RequestBody CourseTO course) {
        Course result = coursesService.saveCourse(conversionService.convert(course, Course.class));

        return conversionService.convert(result, CourseTO.class);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteCourse(@PathVariable int id) {
        coursesService.deleteById(id);
    }
}
