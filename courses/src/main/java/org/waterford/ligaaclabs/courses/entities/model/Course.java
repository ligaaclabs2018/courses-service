package org.waterford.ligaaclabs.courses.entities.model;

import javax.persistence.*;
import java.util.List;

/*
This will mark this class as an JPA Entity which has a DB table associated with it
 */
@Entity
/*
The actual table name. Table, column and sequence names will be altered by
org.waterford.ligaaclabs.courses.entities.config.PhysicalNamingStrategyImpl
*/
@Table(name = "Courses")
public class Course {

    /*
    The annotations will mark this field as the id to be used by the jpa CRUD repository and will rely on the existing
    DB sequence to generate the id value if not defined
     */
    @SequenceGenerator(allocationSize = 1, sequenceName = "coursesIdSeq", name = "coursesIdSeq")
    @GeneratedValue(generator = "coursesIdSeq", strategy = GenerationType.SEQUENCE)
    @Id
    private int courseId;

    /*
    This will link the Course entity to the Subject entity based on the subject_id column
     */
    @ManyToOne
    @JoinColumn(name = "subjectId")
    private Subject subject;

    private String teacher;

    /*
    This will link Course entities using a join table - courses2students
     */
    @ManyToMany
    @JoinTable(name = "courses2students",
            joinColumns = @JoinColumn(name = "courseId"),
            inverseJoinColumns = @JoinColumn(name = "studentId")
    )
    private List<Student> students;

    public Course() {

    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
