package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Course;
import org.waterford.ligaaclabs.courses.webapp.transport.CourseTO;

import javax.inject.Inject;
import java.util.stream.Collectors;

@Component
public class Course2CourseTOConverter implements Converter<Course, CourseTO> {
    @Inject
    private Student2StudentTOConverter studentTOConverter;
    @Inject
    private Subject2SubjectTOConverter subjectTOConverter;

    @Override
    public CourseTO convert(Course course) {
        CourseTO result = new CourseTO();
        result.setId(course.getCourseId());
        result.setSubject(subjectTOConverter.convert(course.getSubject()));
        result.setTeacher(course.getTeacher());
        result.setStudents(course.getStudents().stream().map(studentTOConverter::convert).collect(Collectors.toList()));
        return result;
    }
}
