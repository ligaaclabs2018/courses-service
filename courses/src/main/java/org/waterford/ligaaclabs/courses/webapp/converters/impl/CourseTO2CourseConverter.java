package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Course;
import org.waterford.ligaaclabs.courses.entities.model.Student;
import org.waterford.ligaaclabs.courses.entities.model.Subject;
import org.waterford.ligaaclabs.courses.webapp.transport.CourseTO;

import java.util.stream.Collectors;

@Component
public class CourseTO2CourseConverter implements Converter<CourseTO, Course> {

    @Override
    public Course convert(CourseTO courseTO) {
        Course result = new Course();
        result.setCourseId(courseTO.getId());

        Subject subject = new Subject();
        subject.setSubjectId(courseTO.getSubject().getId());
        result.setSubject(subject);

        result.setTeacher(courseTO.getTeacher());

        if (courseTO.getStudents() != null) {
            result.setStudents(courseTO.getStudents().stream().map(student -> {
                Student studentResult = new Student();
                studentResult.setStudentId(student.getId());
                return studentResult;
            }).collect(Collectors.toList()));
        }
        return result;
    }
}
