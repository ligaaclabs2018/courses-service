package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Subject;
import org.waterford.ligaaclabs.courses.webapp.transport.SubjectTO;

@Component
public class SubjectTO2SubjectConverter implements Converter<SubjectTO, Subject> {

    @Override
    public Subject convert(SubjectTO subject) {
        Subject result = new Subject();
        result.setSubjectId(subject.getId());
        result.setName(subject.getName());
        return result;
    }
}
