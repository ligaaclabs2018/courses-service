package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Subject;
import org.waterford.ligaaclabs.courses.webapp.transport.SubjectTO;

@Component
public class Subject2SubjectTOConverter implements Converter<Subject, SubjectTO> {
    @Nullable
    @Override
    public SubjectTO convert(Subject subject) {
        SubjectTO result = new SubjectTO();
        result.setId(subject.getSubjectId());
        result.setName(subject.getName());
        return result;
    }
}
