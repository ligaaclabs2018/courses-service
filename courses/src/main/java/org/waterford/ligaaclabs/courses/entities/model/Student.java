package org.waterford.ligaaclabs.courses.entities.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
This will mark this class as an JPA Entity which has a DB table associated with it
 */
@Entity
/*
The actual table name. Table, column and sequence names will be altered by
org.waterford.ligaaclabs.courses.entities.config.PhysicalNamingStrategyImpl
*/
@Table(name = "Students")
public class Student {

    /*
    The annotations will mark this field as the id to be used by the jpa CRUD repository and will rely on the existing
    DB sequence to generate the id value if not defined
     */
    @SequenceGenerator(allocationSize=1, sequenceName="studentsIdSeq", name="studentsIdSeq")
    @GeneratedValue(generator="studentsIdSeq", strategy=GenerationType.SEQUENCE)
    @Id
    private int studentId;

    private String email;
    private String firstName;
    private String lastName;

    @ManyToMany(mappedBy = "students")
    private List<Course> courses = new ArrayList<>();

    public Student() {
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
