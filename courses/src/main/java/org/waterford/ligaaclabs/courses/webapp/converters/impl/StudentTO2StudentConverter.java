package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Student;
import org.waterford.ligaaclabs.courses.webapp.transport.StudentTO;

@Component
public class StudentTO2StudentConverter implements Converter<StudentTO, Student> {
    @Override
    public Student convert(StudentTO student) {
        Student result = new Student();
        result.setStudentId(student.getId());
        result.setEmail(student.getEmail());
        result.setFirstName(student.getFirstName());
        result.setLastName(student.getLastName());
        return result;
    }
}
