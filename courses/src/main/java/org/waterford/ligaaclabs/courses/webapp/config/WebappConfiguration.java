package org.waterford.ligaaclabs.courses.webapp.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.waterford.ligaaclabs.courses.webapp.converters.config.ConvertersConfig;

/*
Will mark this class as a configuration one. Configurations can be made using annotation, implementing configuration
interfaces or by defining bean providing methods ( See @Bean )
*/
@Import(ConvertersConfig.class)
@ComponentScan("org.waterford.ligaaclabs.courses.webapp.controllers.*")
public class WebappConfiguration {
}
