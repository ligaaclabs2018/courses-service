package org.waterford.ligaaclabs.courses.entities.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
Will mark this class as a configuration one. Configurations can be made using annotation, implementing configuration
interfaces or by defining bean providing methods ( See @Bean )
*/
@Configuration
/*
Package were to scan for JPA entities
 */
@EntityScan("org.waterford.ligaaclabs.courses.entities.model")
/*
Package were to scan for JPA repositories
 */
@EnableJpaRepositories("org.waterford.ligaaclabs.courses.entities.repositories")
public class EntitiesConfiguration {
}
