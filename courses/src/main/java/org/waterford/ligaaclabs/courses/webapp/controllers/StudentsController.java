package org.waterford.ligaaclabs.courses.webapp.controllers;

import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import org.waterford.ligaaclabs.courses.entities.model.Student;
import org.waterford.ligaaclabs.courses.service.StudentsService;
import org.waterford.ligaaclabs.courses.webapp.exceptions.StudentNotFoundException;
import org.waterford.ligaaclabs.courses.webapp.transport.StudentTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequestMapping("/students")
@RestController
public class StudentsController {
    @Inject
    private StudentsService studentsService;
    @Inject
    private ConversionService conversionService;


    @GetMapping
    public List<StudentTO> getAll() {
        return studentsService.getAll().stream().map(student -> conversionService.convert(student, StudentTO.class)).collect(Collectors.toList());
    }

    @GetMapping(path = "/{id}")
    public StudentTO getById(@PathVariable int id) {
        Optional<Student> student = studentsService.getById(id);
        if (!student.isPresent()) {
            throw new StudentNotFoundException();
        }
        return conversionService.convert(student.get(), StudentTO.class);
    }

    @GetMapping(path = "/course/{courseId}")
    public List<StudentTO> getAllByCourseId(@PathVariable int courseId) {
        return studentsService.getAllByCourseId(courseId).stream().map(student -> conversionService.convert(student, StudentTO.class)).collect(Collectors.toList());
    }

    @PutMapping
    public StudentTO saveStudent(@RequestBody StudentTO student) {
        Student result = studentsService.saveStudent(conversionService.convert(student, Student.class));
        return conversionService.convert(result, StudentTO.class);
    }

    @PostMapping
    public List<StudentTO> saveStudents(@RequestBody List<StudentTO> students) {
        List<Student> result = studentsService.saveStudents(students.stream().map(student -> conversionService.convert(student, Student.class)).collect(Collectors.toList()));
        return result.stream().map(student -> conversionService.convert(student, StudentTO.class)).collect(Collectors.toList());
    }

    @DeleteMapping(path = "/{id}")
    public void deleteStudent(@PathVariable int id) {
        studentsService.deleteById(id);
    }
}
