package org.waterford.ligaaclabs.courses.entities.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.waterford.ligaaclabs.courses.entities.model.Student;

@Repository
public interface StudentsRepository extends JpaRepository<Student, Integer> {

}
