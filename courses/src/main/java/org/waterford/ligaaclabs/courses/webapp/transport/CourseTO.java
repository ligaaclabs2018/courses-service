package org.waterford.ligaaclabs.courses.webapp.transport;

import java.util.List;

public class CourseTO {
    private int id;
    private SubjectTO subject;
    private String teacher;

    private List<StudentTO> students;

    public CourseTO(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SubjectTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectTO subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public List<StudentTO> getStudents() {
        return students;
    }

    public void setStudents(List<StudentTO> students) {
        this.students = students;
    }
}
