package org.waterford.ligaaclabs.courses.service;

import org.waterford.ligaaclabs.courses.entities.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentsService {
    List<Student> getAll();

    Optional<Student> getById(int id);

    List<Student> getAllByCourseId(int courseId);

    Student saveStudent(Student student);

    List<Student> saveStudents(List<Student> students);

    void deleteById(int id);
}
