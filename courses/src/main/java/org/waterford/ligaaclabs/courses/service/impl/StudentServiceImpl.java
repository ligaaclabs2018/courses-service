package org.waterford.ligaaclabs.courses.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.waterford.ligaaclabs.courses.entities.model.Course;
import org.waterford.ligaaclabs.courses.entities.model.Student;
import org.waterford.ligaaclabs.courses.entities.repositories.CoursesRepository;
import org.waterford.ligaaclabs.courses.entities.repositories.StudentsRepository;
import org.waterford.ligaaclabs.courses.service.StudentsService;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentsService {
    @Inject
    private StudentsRepository studentsRepository;
    @Inject
    private CoursesRepository coursesRepository;

    @Override
    public List<Student> getAll() {
        return studentsRepository.findAll();
    }

    @Override
    public Optional<Student> getById(int id) {
        return studentsRepository.findById(id);
    }

    @Override
    public List<Student> getAllByCourseId(int courseId) {
        Optional<Course> course = coursesRepository.findById(courseId);
        if (course.isPresent()) {
            return course.get().getStudents();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    @Transactional
    public Student saveStudent(Student student) {
        return studentsRepository.save(student);
    }

    @Override
    @Transactional
    public List<Student> saveStudents(List<Student> students) {
        return studentsRepository.saveAll(students);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        studentsRepository.deleteById(id);
    }
}
