package org.waterford.ligaaclabs.courses.entities.model;

import javax.persistence.*;

/*
This will mark this class as an JPA Entity which has a DB table associated with it
 */
@Entity
/*
The actual table name. Table, column and sequence names will be altered by
org.waterford.ligaaclabs.courses.entities.config.PhysicalNamingStrategyImpl
*/
@Table(name = "Subjects")
public class Subject {

    /*
    The annotations will mark this field as the id to be used by the jpa CRUD repository and will rely on the existing
    DB sequence to generate the id value if not defined
     */
    @SequenceGenerator(allocationSize = 1, sequenceName="subjectsIdSeq", name="subjectsIdSeq")
    @GeneratedValue(generator="subjectsIdSeq", strategy=GenerationType.SEQUENCE)
    @Id
    private int subjectId;

    private String name;

    public Subject() {
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
