package org.waterford.ligaaclabs.courses.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.waterford.ligaaclabs.courses.entities.model.Course;
import org.waterford.ligaaclabs.courses.entities.repositories.CoursesRepository;
import org.waterford.ligaaclabs.courses.service.CoursesService;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@Service
public class CoursesServiceImpl implements CoursesService {
    @Inject
    private CoursesRepository coursesRepository;

    @Override
    public List<Course> getAll() {
        return coursesRepository.findAll();
    }

    @Override
    public Optional<Course> getById(int id) {
        return coursesRepository.findById(id);
    }

    @Override
    public List<Course> getBySubjectId(int subjectId) {
        return coursesRepository.findAllBySubjectSubjectId(subjectId);
    }

    @Override
    public List<Course> getByTeacher(String teacher) {
        return coursesRepository.findAllByTeacher(teacher);
    }

    @Override
    @Transactional
    public Course saveCourse(Course course) {
        /*
        If students are not defined in the input entity do not update them
         */
        if(course.getStudents() == null) {
            Optional<Course> toUpdate = coursesRepository.findById(course.getCourseId());
            toUpdate.ifPresent(courseToUpdate -> course.setStudents(courseToUpdate.getStudents()));
        }
        return coursesRepository.save(course);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        coursesRepository.deleteById(id);
    }
}
