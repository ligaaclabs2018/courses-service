package org.waterford.ligaaclabs.courses.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.courses.entities.model.Student;
import org.waterford.ligaaclabs.courses.webapp.transport.StudentTO;

@Component
public class Student2StudentTOConverter implements Converter<Student, StudentTO> {
    @Override
    public StudentTO convert(Student student) {
        StudentTO result = new StudentTO();
        result.setId(student.getStudentId());
        result.setEmail(student.getEmail());
        result.setFirstName(student.getFirstName());
        result.setLastName(student.getLastName());
        return result;
    }
}
