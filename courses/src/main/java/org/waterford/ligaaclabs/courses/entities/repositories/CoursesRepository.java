package org.waterford.ligaaclabs.courses.entities.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.waterford.ligaaclabs.courses.entities.model.Course;

import java.util.List;

@Repository
public interface CoursesRepository extends JpaRepository<Course, Integer> {
    /*
        select where subject id equals param
     */
    List<Course> findAllBySubjectSubjectId(int subjectId);

    List<Course> findAllByTeacher(String teacher);
}
